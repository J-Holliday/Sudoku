using UnityEngine;
using System.Text.RegularExpressions;
using Sudoku.SudokuGenerator.Runner;

namespace Sudoku.MVC {
/**
 * Sudoku view component.
 * Updates the view with data from the SudokuModel.
 */
public class SudokuView : MonoBehaviour {
	[SerializeField]
	private static FieldHandler[] fieldHandlers = null;
	
	/**
     * Awake.
     * Awake is the first function called by this script, it gets reference to all of the field handlers.
     */
    void Awake() {
		// TODO implement here
		GameObject[] sudokuFieldObjects = GameObject.FindGameObjectsWithTag ("SudokuField");
		fieldHandlers = new FieldHandler[sudokuFieldObjects.Length];

		for (int i = 0; i < sudokuFieldObjects.Length; i++) {
			sudokuFieldObjects[i].AddComponent<FieldHandler>();

			int field = int.Parse(Regex.Replace(sudokuFieldObjects[i].ToString(), "[^0-9]", ""));
			int cluster = int.Parse(Regex.Replace(sudokuFieldObjects[i].transform.parent.gameObject.ToString(), "[^0-9]", ""));
			int index = ConvertField(field, cluster);

			fieldHandlers[index] = sudokuFieldObjects[i].GetComponent<FieldHandler>();
			fieldHandlers[index].setFieldIndex(index);

		}
		LevelController.setSudokuLevelVeryHard();

	}

	/**
	 * Initialize the view.
	 * Updates the view with locked fieldhandlers, beginning the level setup.
     * @param sudoku The SudokuModel we are getting data from.
     */
	public static void InitializeView(int[] initialData) {
		// TODO implement here
		if (fieldHandlers != null) {
			for (int i = 0; i < fieldHandlers.Length; i++) {
				fieldHandlers [i].setValueLocked (initialData[i]);
			}
		}
	}
	

    /**
     * Win handler.
     * Call on win to handle a completed Sudoku.
     */
    public static void DisplayWin() {
        // TODO implement here
		print ("win");
    }

	/**
	 * Reference to model field.
	 * Converts the referenced field and cluster to a field value appropriate for our model.
	 */
	private int ConvertField(int field, int cluster){
		int index = 0;
		if ((cluster+3)%3 == 0) {
			if (field < 3){
				index = (9 * cluster) + field;
			} else if (field < 6) {
				index = ((9 * (cluster+1))) + field -3;
			} else {
				index = ((9 * (cluster+2))) + field -6;

			}
		}
		else if ((cluster+2)%3 == 0) {
			if (field < 3){
				index = (9 * (cluster-1)) + field +3;
			} else if (field < 6) {
				index = (9 * (cluster)) + field;
			} else {
				index = (9 * (cluster+1)) + field -3;
			}
		}
		else if ((cluster+1)%3 == 0) {
			if (field < 3){
				index = (9 * (cluster-2)) + field +6;
			} else if (field < 6) {
				index = (9 * (cluster-1)) + field +3;
			} else {
				index = (9 * (cluster)) + field;
			}
		}
		return index;
	}
}
}