using UnityEngine;

namespace Sudoku.MVC {
/**
 * Sudoku controller component.
 * Manipulates the data within a SudokuModel.
 */
public static class SudokuController {
    
	[SerializeField]
	private static SudokuModel sudokuModel; /**< The SudokuModel being handled. */
	[SerializeField]
	private static int[] initialData; /**< The initial data for the puzzle start point. */

	/**
	 * Setter for initialData.
	 * Sets initialData and sets up a new SudokuModel.
	 * @param int[] array for initial data.
	 */
	public static void setInitialData(int[] values){
		initialData = values;
		sudokuModel = new SudokuModel (initialData);
		SudokuView.InitializeView (initialData);
		if (sudokuModel.getWin ()) {
				SudokuView.DisplayWin();
			}
	}

	/**
	 * Getter for the SudokuModel.
	 * If we have a SudokuModel created, return the SudokuModel, else return a new one
	 * with no data.
	 */
	public static SudokuModel getSudokuModel() {
		if (null != sudokuModel) {
			return sudokuModel;
		} else {
			return new SudokuModel (new int[81]);
		}
	}

	/**
     * Field update handler.
     * Passes field updates to the SudokuModel.
     * @param fieldIndex index to set the data at.
     * @param value int value to set data to.
     * @return bool true if function success.
     */
	public static bool HandleInput(int fieldIndex, int value) {
		if (null != sudokuModel) {
			sudokuModel.setTableValueAt(fieldIndex, value);
				Debug.Log (sudokuModel.getCompletedRowAt(0));
				if (sudokuModel.getWin ()) {
					SudokuView.DisplayWin();
				}
			return true;
		} else {
		return false;
		}
	}

	/**
	 * Reset values to initialData.
	 * Overwrites the values in SudokuModel to the values in initialData.
	 * @return bool true if function success.
	 */
	public static bool ResetModel(){
		if (null != initialData) {
			setInitialData(initialData);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Set initialData to null.
	 * Nulls the reference to our initial data array.
	 */
	public static void ClearData(){
		initialData = null;
		sudokuModel = null;
	}
}
}