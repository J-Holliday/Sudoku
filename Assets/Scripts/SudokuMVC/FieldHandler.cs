﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Sudoku.MVC {
/**
 * Sudoku field handler class.
 * Script for handling UI fields within the scene.
 */
public class FieldHandler : MonoBehaviour {

	[SerializeField]
	private int value = 0; /**< The value we are storing to display on the field. */

	[SerializeField]
	private int fieldIndex = 0; /**< The fieldIndex the field is at. */

	[SerializeField]
	private bool locked = false /**< Bool determining the locked state of the field. */;

	[SerializeField]
	private Image image = null; /**< The Image component of the field. */

	[SerializeField]
	private Text text = null; /**< The Text component of the field. */

	[SerializeField]
	private Button button = null; /**< The Button component of the field. */

	/** 
	 * Setup components. 
	 * Sets up the components required by this script on initialization.
	 */
	void Awake () {
		text = this.GetComponentInChildren<Text> ();
		image = this.GetComponent<Image> ();
		button = this.GetComponent<Button> ();
		button.onClick.AddListener (() => HandleClick ());
	}

	/**
	 * Setter for fieldIndex.
	 * Sets the value of fieldIndex.
	 * @param index the int to set fieldIndex to.
	 */
	public void setFieldIndex (int index)
	{
		this.fieldIndex = index;
	}

	/**
	 * Setter for text.
	 * Sets the value of this fields Text component, and performs validation.
	 * @param value the int to set text to.
	 */
	private void setText (int value)
	{
		if (null != this.text)
		{
			if (value == 0 || value > 9)
			{
				text.text = "";
			}
			else
			{
				text.text = value.ToString ();
			}
		}
	}
	
	/**
	 * Setter for value.
	 * Sets value and text to the passed int.
	 * @param value the int to set.
	 */
	public void setValue (int value)
	{
		this.value = value;
		this.setText (value);
		
	}
	
	/**
	 * Setter for value.
	 * Locks the value after setting it, and performs UI manipulation to indicate this.
	 * @param value the int to set.
	 */
	public void setValueLocked (int value)
	{
		if (value != 0 && value != 10)
		{
			this.value = value;
			this.setText (value);
			this.image.color = new Color32 (128, 87, 199, 131);
			this.text.color = Color.white;
			this.locked = true;
		}
		else
		{
			this.image.color = Color.white;
			this.text.color = Color.black;
			this.value = value;
			this.setText (value);
			this.locked = false;
		}
	}

	/**
	 * Input Handler.
	 * Sends input to the SudokuController. 
	 */
	public void HandleInput ()
	{
		SudokuController.HandleInput (this.fieldIndex, this.value);
	}

	/**
	 * onClick handler.
	 * Handles onClick events on this fields Button component.
	 */
	private void HandleClick ()
	{
		if (!this.locked)
		{
			this.StartCoroutine (this.WaitForKeyDown ());
		}
	}

	/**
	 * Wait for key press.
	 * Waits for a key press, and if the key is valid sets the value to it.
	 * @param value the int to set.
	 */
	private IEnumerator WaitForKeyDown (){
		while (!Input.anyKeyDown) {
			yield return null;
		}
		int parse = 0;
		int.TryParse (Input.inputString.ToString (), out parse);

		setValue (parse);
			HandleInput ();

	}
}
}