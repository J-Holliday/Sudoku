using System;

/**
 * Sudoku model component.
 * Contains data which can be manipulated for updating the view.
 */
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class SudokuModel{
    
	private int[] tableValues = new int[81]; /**< int[] for storing table values. */
	private int[] tableValuesBacktracking = new int[81];
	private bool[] completedRows = new bool[9]; /**< bool[] for storing values of rows. */
	private bool[] completedColumns = new bool[9]; /**< bool[] for storing values of columns. */
	private bool[] completedSquares = new bool[9]; /**< bool[] for storing values of squares. */
	private bool complete = false; /**< bool for storing whether the SudokuModel is complete or not. */
	private bool incorrect = true;
	private readonly byte[] possibleValues;


	/**
	 * Constructor for the SudokuModel.
	 * Ensure we initialise SudokuModel with tableValues[].
	 * @param values[] int array to set tableValues to.
	 */
	public SudokuModel(int[] values){
		this.possibleValues = Enumerable.Range(1, 9).Select(value => (byte)value).ToArray();
		setTableValues (values);
	}

    /**
     * Setter for tableValues with array.
     * Sets the tableValues int[] to the int[] values, and performs validation.
     * @param values[] int array to set tableValues to.
     */
	public void setTableValues(int[] values) {
		/** Validation checks for oversized content and handling different values[] sizes. */
		if (tableValues.Length <= values.Length) {
			for (int i = 0; i < tableValues.Length; i++) {

				if (values [i] < 10) {
					tableValues [i] = values [i];
				} else {
					tableValues [i] = 10;
				}
			} 
		} else {
			for (int i = 0; i < values.Length; i++) {
				if (values [i] < 10) {
					tableValues [i] = values [i];
				} else {
					tableValues [i] = 10;
				}
			}
		}

		/** Checks for all valid completed sets of Sudoku row/column values, and writes them as true
		 	* at their respective array locations. */
		int tableRow = 0;
		for (int i = 0; i < 9; i++) {

			int[] row = new int[9]{0, 1, 2, 3, 4, 5, 6, 7, 8}; /** Check rows. */

			/** Generate row to check. */
			if (0 != i) {
				for (int j = 0; j < 9; j++) {
					row [j] = ((j) + (i * 9));
				}
			}

			/** Check generated row. */
			if (CheckSet (row)) {
				setCompletedRowAt (i, true);
			} else {
				setCompletedRowAt (i, false);
			}

			int[] column = new int[9]{0, 9, 18, 27, 36, 45, 54, 63, 72}; /** Check columns. */

			/** Generate column to check. */
			if (0 != i) {
				for (int k = 0; k < 9; k++) {
					column [k] = (i + (k * 9));
				}
			}

			/** Check generated column. */
			if (CheckSet (column)) {
				setCompletedColumnAt (i, true);
			} else {
				setCompletedColumnAt (i, false);
			}

			/** Generate square to check. */
			int[] square = new int[9]{0, 1, 2, 9, 10, 11, 18, 19, 20};
			if (i != 0) {
				for (int s = 0; s < 9; s++) {
					if (s < 3) {
						square [s] = tableRow * 27 + ((s + 3) % 3) + (((i + 3) % 3) * 3);
					} else if (s < 6) {
						square [s] = tableRow * 27 + ((s + 3) % 3) + (((i + 3) % 3) * 3) + 9;
					} else {
						square [s] = tableRow * 27 + ((s + 3) % 3) + (((i + 3) % 3) * 3) + 18;
					}
				}
				if (((i + 3) % 3) == 2) {
					tableRow++;
				}
			}
			
			/** Check generated square. */
			if (CheckSet (square)) {
				setcompletedSquareAt (i, true);
			} else {
				setcompletedSquareAt (i, false);
			}
		}
	}
	
    /**
     * Setter for tableValues with single value.
     * Sets a value in int[] tableValues to input, and performs validation.
     * @param fieldIndex int index to set value at. 
     * @param input int value to set to.
     */
    public void setTableValueAt(int fieldIndex, int input) {
		if (tableValues.Length > fieldIndex) {
			tableValues [fieldIndex] = input; 
			//setTableValues (tableValues);
			if (input < 10) {
				tableValues [fieldIndex] = input;
			} else {
				tableValues [fieldIndex] = 10; 
			}

			/** Get column at fieldIndex. */
			int columnindex = fieldIndex % 9;
			int[] column = new int[9]; 
			for (int i = columnindex; i < 81; i=i+9) {
				column [(i - columnindex) / 9] = i;
			}

			/** Check column at fieldIndex. */ 
			if (CheckSet (column)) {
				setCompletedColumnAt(columnindex, true);
			} else {
				setCompletedColumnAt(columnindex, false);
			}

			/** Get row at fieldIndex. */
			int[] row = new int[9]; 
			int rowindex = (fieldIndex - columnindex);
			for (int j = 0; j < 9; j++) {
				row [j] = j + (rowindex);
			}
			/** Check row at fieldIndex. */
			if (CheckSet (row)) {
				setCompletedRowAt(rowindex/9, true);
			} else {
				setCompletedRowAt(rowindex/9, false);
			}

			/** Get square at field index. */
			int[] square = new int[9]{0, 1, 2, 9, 10, 11, 18, 19, 20};
			int startValue = (fieldIndex - ((columnindex%3) + (((rowindex/9)%3)*9)));
			int squareindex = 0;
			int tableRow = 0;

			/** get squareindex. */
			if (startValue < 27) {
				squareindex = ((startValue + 3)/3)-1;
			} else if (startValue < 54) {
				squareindex = (((startValue-27)+3)/3)+2;
			} else {
				squareindex = (((startValue-54)+3)/3)+5;

			}

			/** Get the row the square is on. */
			if (squareindex < 3){
				tableRow = 0;
			} else if (squareindex < 6){
				tableRow = 1;
			}
			else{
				tableRow = 2;
			}

			/** Generate the square. */
			for (int s = 0; s < 9; s++) {
				if (s < 3) {
					square [s] = tableRow * 27 + ((s + 3) % 3) + (((squareindex + 3) % 3) * 3);	
				} else if (s < 6) {
					square [s] = tableRow * 27 + ((s + 3) % 3) + (((squareindex + 3) % 3) * 3) + 9;
				} else {
					square [s] = tableRow * 27 + ((s + 3) % 3) + (((squareindex + 3) % 3) * 3) + 18;
				}
			}

			/** Check generated square. */
			if (CheckSet (square)) {
				setcompletedSquareAt (squareindex, true);
			} else {
				setcompletedSquareAt (squareindex, false);
			}
		}
	}

    /**
     * Getter for tableValues by index.
     * Returns an int from tableValue at the index fieldIndex.
     * @param fieldIndex int index to get value at.
     * @return int value at index.
     */
    public int getTableValueAt(int fieldIndex) {
		if (tableValues.Length > fieldIndex) {
			return tableValues[fieldIndex];
		} else {
			return 0;
		}
    }

	public int[] getTableValues() {
		return tableValues;
	}

    /**
     * Getter for row completion value.
     * Returns a bool indicating whether the row at rowIndex is a 
     * completed row or not.
     * @param rowIndex int index to get value at.
     * @return bool value at index.
     */
    public bool getCompletedRowAt(int rowIndex) {
		if (completedRows.Length > rowIndex) {
			return completedRows [rowIndex];
		} else {
			return false;
		}
    }

	/**
	 * Setter for the completedRow array.
	 * Sets the completedRow array at the index to the passed value, then checks whether the solution is
	 * complete or not.
	 */
	private void setCompletedRowAt(int rowIndex, bool value){
		completedRows [rowIndex] = value;
		/** Check whether the table is complete or not and sets the bool. */
		for (int i = 0; i < 9; i++){
			if (getCompletedColumnAt(i) && getCompletedRowAt(i) && getCompletedSquareAt(i)){
				complete = true;
			} else {
				complete = false;
			}
		}
	}

    /**
     * Getter for column completion value.
     * Returns a bool indicating whether the column at column index is 
     * a completed column or not.
     * @param columnIndex int index to get value at.
     * @return bool value at index.
     */
    public bool getCompletedColumnAt(int columnIndex) {
		if (completedColumns.Length > columnIndex) {
			return completedColumns [columnIndex];
		} else {
			return false;
		}
    }

	/**
	 * Setter for the completedColumn array.
	 * Sets the completedColumn array at the index to the passed value, then checks whether the solution is
	 * complete or not.
	 */
	private void setCompletedColumnAt(int columnIndex, bool value){
		completedColumns [columnIndex] = value;

		/** Check whether the table is complete or not and sets the bool. */
		for (int i = 0; i < 9; i++){
			if (getCompletedColumnAt(i) && getCompletedRowAt(i) && getCompletedSquareAt(i)){
				complete = true;
			} else {
				complete = false;
			}
		}
	}

	/**
     * Getter for square completion value.
     * Returns a bool indicating whether the square at squareIndex is a 
     * completed square or not.
     * @param squareIndex int index to get value at.
     * @return bool value at index.
     */
	public bool getCompletedSquareAt(int squareIndex) {
		if (completedSquares.Length > squareIndex) {
			return completedSquares [squareIndex];
		} else {
			return false;
		}
	}

	/**
	 * Setter for the completedSquare array.
	 * Sets the completedSquare array at the index to the passed value, then checks whether the solution is
	 * complete or not.
	 */
	private void setcompletedSquareAt(int squareIndex, bool value){
		completedSquares [squareIndex] = value;

		/** Check whether the table is complete or not and sets the bool. */
		for (int i = 0; i < 9; i++){
			if (getCompletedColumnAt(i) && getCompletedRowAt(i) && getCompletedSquareAt(i)){
				complete = true;
			} else {
				complete = false;
				break;
			}
		}
	}

	/**
	 * Getter for complete.
	 * Returns whether a Sudoku puzzle is completed or not.
     */
	public bool getWin() {
		return complete;
	}

	/**
	 * Determines row or column validity.
	 * Checks a set of values in tableValues[] for their validity as a 
	 * complete column/row in Sudoku.
	 */
	private bool CheckSet(int[] checkHere){
		/** Checks that their are no duplicate values in the set. */
		int[] validFlags = new int[11];
		for (int i = 0; i < checkHere.Length; i++) {
			if (validFlags[tableValues[checkHere[i]]] != 1){
				validFlags[tableValues[checkHere[i]]] = 1;
			} else if (tableValues[checkHere[i]] == 0) { 
				incorrect = false;
				return false;
			} else {
				incorrect = true;
				return false;
			}
		}

		/** Check that flags 0 or 10 have not been set within the set. */
		if (1 == validFlags [0] || 1 == validFlags [10]) {
			return false;
		} else {
			incorrect = false;
			return true;
		}
	}

	public bool CanSetValue(int fieldIndex, byte value) {
		int oldValue = getTableValueAt (fieldIndex);
		setTableValueAt(fieldIndex, value);
		if (incorrect == true) {
			setTableValueAt(fieldIndex, oldValue);
			incorrect = true;
			return false;
		} else {
			setTableValueAt(fieldIndex, oldValue);
			incorrect = true;
			return true;
		}
	}
		
		// Return a list of bytes.
		public IList<byte> GetPossibleValues(int fieldIndex)
		{
			// Returns results where values can be set.
			return this.possibleValues.Where(value => CanSetValue(fieldIndex, value)).ToList();
		}
}
