﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Sudoku.MVC;
using TrueMagic.SudokuGenerator;
using UnityEngine;

namespace Sudoku.SudokuGenerator.Runner {
public static class LevelController {

	static void setSudokuLevel(int[] value){
		SudokuController.setInitialData (value);
	}

	public static void setSudokuLevelVeryHard(){
					var generator = new Generator();
					var startGenerating = DateTime.Now;
					var sudoku = generator.Generate(3, Level.VeryEasy);

					var startSolving = DateTime.Now;
					var finished = DateTime.Now;
					
						Debug.Log("Generating: " + (startSolving - startGenerating));
						Debug.Log("Solving: " + (finished - startSolving));
						setSudokuLevel(sudoku.Clone());
					
				}
}
}
